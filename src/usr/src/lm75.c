/***********************************************************************************************/
/*         Равота с датчиком температуры                                                       */
/***********************************************************************************************/

//----- Инклуды ---------------------------------------------------------------------------------
#include "lm75.h"

extern     data_type    data; // Структура с различными параметрами устройства
//----- Функции ---------------------------------------------------------------------------------

void ResetI2C(I2C_TypeDef* I2Cx)
{
	uint32_t Timeout = 0x1FF;

	I2C_SoftwareResetCmd(I2Cx, ENABLE);
	while(--Timeout){};
	I2C_SoftwareResetCmd(I2Cx, DISABLE);

	I2C_GenerateSTOP(I2Cx, ENABLE);
	if (I2Cx == I2C1)
	{
		I2C_Configuration(I2Cx,data.i2c1_speed);
	}
	else
	{
		I2C_Configuration(I2Cx,data.i2c2_speed);
	}
}


/*! Функция GetTemperature() − Запуск чтения датчиков температуры.
 */
uint16_t GetTemperature(I2C_TypeDef* I2Cx, uint8_t dev_addres)
{
	uint16_t value = 0;             // Значение температуры
	volatile uint32_t Timeout = 0;  // Время ожидания ошибки
	uint32_t timeout_value;
	if (I2Cx == I2C1)
	{
		timeout_value = 0x1FFF * (5 - data.i2c1_speed);
	}
	else
	{
		timeout_value = 0x1FFF * (5 - data.i2c2_speed);
	}

	// ждем, пока шина осовободится
	Timeout = timeout_value;
	while(I2C_GetFlagStatus(I2Cx, I2C_FLAG_BUSY)) { if (Timeout-- == 0) {ResetI2C(I2Cx); return 1;} }

	I2C_AcknowledgeConfig(I2Cx, ENABLE);

	// Генерируем старт
	I2C_GenerateSTART(I2Cx, ENABLE);
	Timeout = timeout_value;
	while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_MODE_SELECT)) { if (Timeout-- == 0) {ResetI2C(I2Cx); return 1;} }

	// Посылаем адрес датчика
	I2C_Send7bitAddress(I2Cx, dev_addres, I2C_Direction_Transmitter);

	Timeout = timeout_value;
	while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_TRANSMITTER_MODE_SELECTED)) { if (Timeout-- == 0) {ResetI2C(I2Cx); return 1;} }

	I2C_SendData(I2Cx, LM75_REG_TEMP);

	Timeout = timeout_value;
	while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_BYTE_TRANSMITTED)) { if (Timeout-- == 0) {ResetI2C(I2Cx); return 1;} }

	I2C_GenerateSTART(I2Cx, ENABLE);
	Timeout = timeout_value;
	while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_MODE_SELECT)) { if (Timeout-- == 0) {ResetI2C(I2Cx); return 1;} }

	I2C_Send7bitAddress(I2Cx, dev_addres, I2C_Direction_Receiver);

	Timeout = timeout_value;
	while(!I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_RECEIVER_MODE_SELECTED)) { if (Timeout-- == 0) {ResetI2C(I2Cx); return 1;} }

	Timeout = timeout_value;
	while( !I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_BYTE_RECEIVED) ) { if (Timeout-- == 0) {ResetI2C(I2Cx); return 1;} }
	value = I2C_ReceiveData(I2Cx);

	I2C_AcknowledgeConfig(I2Cx, DISABLE);

	Timeout = timeout_value;
	while( !I2C_CheckEvent(I2Cx, I2C_EVENT_MASTER_BYTE_RECEIVED) ) { if (Timeout-- == 0) {ResetI2C(I2Cx); return 1;} }
	value += I2C_ReceiveData(I2Cx) << 8;

	I2C_GenerateSTOP(I2Cx, ENABLE);
	return value;
}

/*! Функция ConvertRAW2TEMP(raw) − Чтение температуры датчика.
* \param uint16_t raw − Данные из датчика
* \return int16_t - значение температуры (0x801B = 27.5 °С)
 */
int16_t ConvertRAW2TEMP(uint16_t raw)
{
	volatile int16_t value;

	if (raw & 0x0080) value = (raw | 0xFFFFFF00) * 10 - (raw >> 15) * 5;
	else              value = (raw & 0x000000FF) * 10 + (raw >> 15) * 5;

	return value;
}
