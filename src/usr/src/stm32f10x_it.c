#include "stm32f10x_it.h"

/***********************************************************************************************/
/*            Cortex-M3 Processor Exceptions Handlers                                          */
/***********************************************************************************************/
extern            data_type   data; // Структура с различными параметрами устройства
extern volatile   uint8_t     cmd;  // команда на исполнение



void NMI_Handler         (void){         }
void HardFault_Handler   (void){while(1);}
void MemManage_Handler   (void){while(1);}
void BusFault_Handler    (void){while(1);}
void UsageFault_Handler  (void){while(1);}
void SVC_Handler         (void){         }
void DebugMon_Handler    (void){         }
void PendSV_Handler      (void){         }
void SysTick_Handler     (void){         }



void USART1_IRQHandler (void)
{
	if ( !(data.flags & FLAG_TEMP_BUSY) )
	{
		cmd = USART1->DR;
		data.flags |= USART1_RECIVE_DATA;
	}
	USART_ClearITPendingBit( USART1, USART_IT_RXNE );
}

void DMA1_Channel4_IRQHandler(void)
{
	if (DMA_GetFlagStatus(DMA1_FLAG_TC4))
	{
        data.flags &= ~FLAG_TEMP_BUSY; // Работа окончена
		DMA_ClearFlag(DMA1_FLAG_TC4);
	}
}

/******************* (C) COPYRIGHT 2010 STMicroelectronics *****END OF FILE****/
