/**
  ******************************************************************************
  * @file    main.c
  * @author  ZDA
  * @version V1
  * @date
  * @brief   Main program body
  ******************************************************************************
  * @attention
  *
  *
  * <h2><center>&copy; COPYRIGHT 2015 </center></h2>
  ******************************************************************************
  */

/* Includes ------------------------------------------------------------------*/

#include "main.h"


volatile    data_type   data; // структура с данными
volatile    uint8_t     cmd;  // команда на исполнение
            uint8_t     temp_sens_addres[] = { LM75_ADDR_0, LM75_ADDR_1, LM75_ADDR_2, LM75_ADDR_3,
                                               LM75_ADDR_4, LM75_ADDR_5, LM75_ADDR_6, LM75_ADDR_7
                                             };        // Список адресов датчиков температуры
            uint8_t     help_string[] = "Commands\r\n  P - ping - \"pong\"\r\n  \r\n  Enabling and disabling of chosen sensors:\r\n    Command allows you to accelerate the receipt of information from the sensors disconnecting unused;\r\n    0xEX - enable read X temperature sensor (HEX)\r\n    0xDX - disable read X temperature sensor (HEX)\r\n      where X is the number of sensor - 1\r\n    EXAMPLE: \"0xEF\" - enable read 16 temperature sensor; \"0xDA\" - disable read 11 temperature sensor; \r\n    \r\n  Installing the operating modes:\r\n    F - Survey sensors everytime(default)\r\n    Q - Poll sensors on request (F command cancels)\r\n\r\n    D - Sends the temperature in chelovekochitaemom a (slower)\r\n    R - Sends the temperature in the RAW file format (default)\r\n\r\n    Z - detect and ignore errore sensors\r\n    A - exclude the sensors with an error\r\n    B - read all sensors including bad (default)(slower)\r\n	\r\n  Installation of temperature sensors speed:\r\n    H(igh) - I2C sped 400 kHz\r\n    N(ormal) - I2C sped 200 kHz (default)\r\n    M(idle) - I2C sped 100 kHz\r\n    L(ow) - I2C sped 50 kHz\r\n    V(ery low) - I2C sped 10 kHz\r\n    \r\n  Information request:\r\n    G - Get temperature data\r\n    C - Start read temperature data and get\r\n    E - Get tempSens error\r\n    I - Get ID device\0";





//----- Прототипы функций ----------------------------------------------------------------------------------
void temperatureRead(void);

//----- Функции --------------------------------------------------------------------------------------------
/**
  * @brief  Main program
  * @param  None
  * @retval None
  */
int main(void)
{
    uint8_t temp_cmd_data = 0;
    
	Init_STM32100();

	data.flags = FOREVER_RX_TEMP_DATA;

	sendDatatoPC( (uint8_t*) &data.ID, 12 );

	while (1)
	{
		if (data.flags & FOREVER_RX_TEMP_DATA)
		{
			temperatureRead();
		}
		if (data.flags & USART1_RECIVE_DATA)
		{
			data.flags &= ~USART1_RECIVE_DATA;
            if (! ( data.flags & FLAG_TEMP_BUSY) )
            {
                switch (cmd)
                {
                    case 'F':
                    
                        data.flags |=  FOREVER_RX_TEMP_DATA;
                        sendDatatoPC( (uint8_t*) &cmd, 1 );
                        break;
                        
                    case 'Z':
                        data.sensor.sensor_error = 0x0000;
                        temperatureRead();
                        data.ignore_sensor =  data.sensor.sensor_error;
                        sendDatatoPC( (uint8_t*) &cmd, 1 );
                        break;
                        
                    case 'Q':
                        data.flags &= ~FOREVER_RX_TEMP_DATA;
                        sendDatatoPC( (uint8_t*) &cmd, 1 );
                        break;

                    case 'C':
                        temperatureRead();
                        sendDatatoPC( (uint8_t*) &data.sensor.sensor_error, 34 );
                        break;

                    case 'G':
                        sendDatatoPC( (uint8_t*) &data.sensor.sensor_error, 34 );
                        break;

                    case 'E':
                        sendDatatoPC( (uint8_t*) &data.sensor.sensor_error, 2 );
                        break;

                    case 'I':
                        sendDatatoPC( (uint8_t*) &data.ID, 12 );
                        break;
                        
                    case 'P':
                        sendDatatoPC( (uint8_t*)"pong", 4 );
                        break;
                        
                    case 'D':
                        data.flags |= DEC_FORMAT_DATA;
                        sendDatatoPC( (uint8_t*) &cmd, 1 );
                        break;
                        
                    case 'X':
                        NVIC_SystemReset();
                        break;
                        
                    case 'R':
                        data.flags &= ~DEC_FORMAT_DATA;
                        sendDatatoPC( (uint8_t*) &cmd, 1 );
                        break;
                        
                    case 'A':
                        data.flags |= DIS_SENSOR_IF_ERROR;
                        sendDatatoPC( (uint8_t*) &cmd, 1 );
                        break;
                        
                    case 'B':
                        data.flags &= ~DIS_SENSOR_IF_ERROR;
                        data.ignore_sensor = 0x0000;
                        sendDatatoPC( (uint8_t*) &cmd, 1 );
                        break;

                    case 'H':
                        data.i2c1_speed = 4;
                        data.i2c2_speed = 4;
                        I2C_Configuration(I2C1, data.i2c1_speed);     // Настройка модуля I2C1
                        I2C_Configuration(I2C2, data.i2c2_speed);     // Настройка модуля I2C2
                        sendDatatoPC( (uint8_t*) &cmd, 1 );
                        break;

                    case 'N':
                        data.i2c1_speed = 3;
                        data.i2c2_speed = 3;
                        I2C_Configuration(I2C1, data.i2c1_speed);     // Настройка модуля I2C1
                        I2C_Configuration(I2C2, data.i2c2_speed);     // Настройка модуля I2C2
                        sendDatatoPC( (uint8_t*) &cmd, 1 );
                        break;

                    case 'M':
                        data.i2c1_speed = 2;
                        data.i2c2_speed = 2;
                        I2C_Configuration(I2C1, data.i2c1_speed);     // Настройка модуля I2C1
                        I2C_Configuration(I2C2, data.i2c2_speed);     // Настройка модуля I2C2
                        sendDatatoPC( (uint8_t*) &cmd, 1 );
                        break;

                    case 'L':
                        data.i2c1_speed = 1;
                        data.i2c2_speed = 1;
                        I2C_Configuration(I2C1, data.i2c1_speed);     // Настройка модуля I2C1
                        I2C_Configuration(I2C2, data.i2c2_speed);     // Настройка модуля I2C2
                        sendDatatoPC( (uint8_t*) &cmd, 1 );
                        break;

                    case 'V':
                        data.i2c1_speed = 0;
                        data.i2c2_speed = 0;
                        I2C_Configuration(I2C1, data.i2c1_speed);     // Настройка модуля I2C1
                        I2C_Configuration(I2C2, data.i2c2_speed);     // Настройка модуля I2C2
                        sendDatatoPC( (uint8_t*) &cmd, 1 );
                        break;
                        
                    case '?':
                        sendDatatoPC( help_string, sizeof(help_string) );
                        break;
                        
                    default: 
                        temp_cmd_data = cmd & 0xF0;
                        
                        if (temp_cmd_data == 0xD0)
                        {
                            data.ignore_sensor |= (1 << (cmd& 0x0F));
                            sendDatatoPC( (uint8_t *)"ok", 2 );
                        } 
                        else if (temp_cmd_data == 0xE0)
                        {
                            data.ignore_sensor &= ~(1 << (cmd& 0x0F));
                            sendDatatoPC( (uint8_t *)"ok", 2 );
                        }
                        else
                        {
                            sendDatatoPC( (uint8_t *)"unknow command", 14 );
//                            data.flags &= ~FLAG_TEMP_BUSY; // Работа окончена
                        }
                    break;
                }
                cmd = 0x0;
            }
        }
	}
}



// отправляет данные в порт
void sendDatatoPC(uint8_t *buff, uint32_t length)
{
    data.flags |= FLAG_TEMP_BUSY;
	DMA1_Channel4->CCR &= ~DMA_CCR4_EN;    // выключить ДМА
	DMA1_Channel4->CMAR = (uint32_t) buff; // нужно назначить ДМА адрес buff
	DMA1_Channel4->CNDTR = length;         // нужно назначить размер буфера - length
	DMA1_Channel4->CCR |= DMA_CCR4_EN;     // запустить ДМА и выдать всё в порт
}

void temperatureRead(void)
{
	uint8_t i = 0;
	uint16_t temperature_bufer = 0;

	for ( i = 0; i < 8; i++ )
	{
        if (data.ignore_sensor & (1 << i)) continue;
        
		temperature_bufer = GetTemperature( I2C1, temp_sens_addres[ i ] );
		if (temperature_bufer == 1)
		{
			data.sensor.sensor_error |= 1 << i ;
			data.sensor.sensor_value[ i ] = 0;
            if (data.flags & DIS_SENSOR_IF_ERROR) data.ignore_sensor |= (1 << i);
		}
		else
		{
			data.sensor.sensor_value[ i ] = (data.flags & DEC_FORMAT_DATA) ? ConvertRAW2TEMP(temperature_bufer) : temperature_bufer;
			data.sensor.sensor_error &= ~(1 << i );
		}
	}

	for ( i = 0; i < 8; i++ )
	{
        if (data.ignore_sensor & (1 << (i + 8))) continue;
        
		temperature_bufer = GetTemperature( I2C2, temp_sens_addres[ i ] );
		if (temperature_bufer == 1)
		{
			data.sensor.sensor_error |= 1 << (i + 8);
			data.sensor.sensor_value[ i + 8 ] = 0;
            if (data.flags & DIS_SENSOR_IF_ERROR) data.ignore_sensor |= (1 << (i + 8));
		}
		else
		{
			data.sensor.sensor_value[ i + 8 ] = (data.flags & DEC_FORMAT_DATA) ? ConvertRAW2TEMP(temperature_bufer) : temperature_bufer;
			data.sensor.sensor_error &= ~(1 << (i + 8) );
		}
	}
}



#ifdef  USE_FULL_ASSERT

void assert_failed(uint8_t* file, uint32_t line) 
{
    while (1)
    {
        TIM2->ARR = 2000; // Будем часто мигать светодиодом если будет какая-то хардварная ошибка
    }
}

#endif
/******************* (C) COPYRIGHT 2011 STMicroelectronics *****END OF FILE****/
