#include "init_stm32f100.h"



// Список позволительных скоростей для I2С
uint32_t speed_array[] = { I2C_SPEED_VERYLOW,
                           I2C_SPEED_LOW,
						   I2C_SPEED_MIDDLE,
						   I2C_SPEED_NORMAL,
						   I2C_SPEED_HIGH };


extern data_type data; // Структура с различными параметрами устройства


/*---------------- Настройка тактирования ядра ----------------------------- */
void RCC_Configuration(void)
{
	FLASH_PrefetchBufferCmd( FLASH_PrefetchBuffer_Enable );
	// Цикл ожидания = 0
	FLASH_SetLatency( FLASH_Latency_0 );

	// Cброс настроек RCC
	RCC_DeInit();

	// Выключаем HSE и включаем HSI
	RCC_HSEConfig( RCC_HSE_ON );
	RCC_HSICmd( DISABLE );

	RCC_WaitForHSEStartUp();

	RCC_HCLKConfig  ( RCC_SYSCLK_Div1 );
	RCC_PCLK1Config ( RCC_HCLK_Div1   );
	RCC_PCLK2Config ( RCC_HCLK_Div1   );
	RCC_SYSCLKConfig( RCC_SYSCLKSource_HSE );
	RCC_PLLCmd( DISABLE );

	// На всякий случай отключаем системный таймер и его прерывание.
	SysTick->CTRL = 0x00000000;
}

/*-------------------------------------------------------------------------- */
void GPIO_Configuration(void)
{
	GPIO_InitTypeDef GPIO_InitStruct;

	//--------- Включаем клоки ---------------------------------
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_GPIOA       // Порт А
	                      |RCC_APB2Periph_GPIOB       // Порт B
	                      |RCC_APB2Periph_AFIO,       // Альтернативные функции
	                       ENABLE);

	//--------- Настройка выводов для I2C ---------------------
	//	PB6  --> I2C1_SCL
	//	PB7  --> I2C1_SDA
	// ----------------------------
	//	PB10 --> I2C2_SCL
	//	PB11 --> I2C2_SDA
	GPIO_InitStruct.GPIO_Pin   = GPIO_Pin_6 | GPIO_Pin_7 | GPIO_Pin_10 | GPIO_Pin_11;
	GPIO_InitStruct.GPIO_Mode  = GPIO_Mode_AF_OD;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPIOB, &GPIO_InitStruct);


	//--------- Настройка выводов для USART -------------------
	//	PA9  --> USART1_TX
	//	PA10 --> USART1_RX
	// ----------------------------
	GPIO_InitStruct.GPIO_Pin   = GPIO_Pin_9;
	GPIO_InitStruct.GPIO_Mode  = GPIO_Mode_AF_PP;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_Init(GPIOA, &GPIO_InitStruct);
	GPIO_InitStruct.GPIO_Pin   = GPIO_Pin_10;
	GPIO_InitStruct.GPIO_Mode  = GPIO_Mode_IN_FLOATING;
	GPIO_Init(GPIOA, &GPIO_InitStruct);


	GPIO_InitStruct.GPIO_Pin   = GPIO_Pin_2;
	GPIO_InitStruct.GPIO_Speed = GPIO_Speed_2MHz;
	GPIO_InitStruct.GPIO_Mode  = GPIO_Mode_AF_PP;
	GPIO_Init(GPIOA, &GPIO_InitStruct);



}

/*-------------------------------------------------------------------------- */
void I2C_Configuration(I2C_TypeDef* I2Cx, uint8_t speed)
{
	I2C_InitTypeDef I2C_InitStructure;

	assert_param(IS_I2C_ALL_PERIPH(I2Cx));
	assert_param(speed <= (sizeof(speed_array) / sizeof(speed_array[0])));

	// Сбрасываем настроки на всякий случай
	I2C_DeInit(I2Cx);

	// Включаем клок для I2C
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C1, ENABLE);
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_I2C2, ENABLE);

	// Заполняем структуру с настройками
	I2C_InitStructure.I2C_Ack                  = I2C_Ack_Enable;
	I2C_InitStructure.I2C_AcknowledgedAddress  = I2C_AcknowledgedAddress_7bit;
	I2C_InitStructure.I2C_ClockSpeed           = speed_array[speed];
	I2C_InitStructure.I2C_DutyCycle            = I2C_DutyCycle_2;
	I2C_InitStructure.I2C_Mode                 = I2C_Mode_I2C;
	I2C_InitStructure.I2C_OwnAddress1          = 0x00;

	// Инициализация I2C1
	I2C_Init(I2Cx, &I2C_InitStructure);
	// Включаем I2C1
	I2C_Cmd(I2Cx, ENABLE);
}

/*-------------------------------------------------------------------------- */
void USART_Configuration(void)
{
	USART_InitTypeDef USART_InitStructure;

	// Включить клок USART
	RCC_APB2PeriphClockCmd(RCC_APB2Periph_USART1, ENABLE);

	// Заполняем структуру с настройками
	USART_InitStructure.USART_BaudRate            = 57600;
	USART_InitStructure.USART_WordLength          = USART_WordLength_8b;
	USART_InitStructure.USART_StopBits            = USART_StopBits_1;
	USART_InitStructure.USART_Parity              = USART_Parity_No;
	USART_InitStructure.USART_HardwareFlowControl = USART_HardwareFlowControl_None;
	USART_InitStructure.USART_Mode                = USART_Mode_Rx | USART_Mode_Tx;

	// Инициализация USART
	USART_Init(USART1, &USART_InitStructure);

	// Разрешаем работать по передаче с DMA1
	USART_DMACmd(USART1, USART_DMAReq_Tx, ENABLE);

	// Запуск USART1
	USART_Cmd(USART1, ENABLE);

	// Разрешаем прерывания по приёму
	USART_ITConfig(USART1, USART_IT_RXNE, ENABLE);
}

/*-------------------------------------------------------------------------- */
void NVIC_Configuration(void)
{
	NVIC_InitTypeDef  NVIC_InitStructure;

	/* Configure two bits for preemption priority */
	NVIC_PriorityGroupConfig(NVIC_PriorityGroup_2);

	// Включаем прерывания по UART1
	NVIC_InitStructure.NVIC_IRQChannel                    = USART1_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority  = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority         = 2;
	NVIC_InitStructure.NVIC_IRQChannelCmd                 = ENABLE;
	NVIC_Init(&NVIC_InitStructure);

	// Включаем прерывания по UART2
	NVIC_InitStructure.NVIC_IRQChannel                    = USART2_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority  = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority         = 2;
	NVIC_InitStructure.NVIC_IRQChannelCmd                 = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
    
    	// Включаем прерывания по UART2
	NVIC_InitStructure.NVIC_IRQChannel                    = DMA1_Channel4_IRQn;
	NVIC_InitStructure.NVIC_IRQChannelPreemptionPriority  = 0;
	NVIC_InitStructure.NVIC_IRQChannelSubPriority         = 2;
	NVIC_InitStructure.NVIC_IRQChannelCmd                 = ENABLE;
	NVIC_Init(&NVIC_InitStructure);
}

void DMA_Configuration(void)
{
	DMA_InitTypeDef DMA1_Init;
	// Включаем клок DMA
	RCC_AHBPeriphClockCmd(RCC_AHBPeriph_DMA1, ENABLE);

	// ----- Настройка DMA для UART --------
	// Выключаем DMA Channel4 на всякий случай
	DMA_Cmd(DMA1_Channel4, DISABLE);
	// Заполнение структуры с настройками
	DMA1_Init.DMA_PeripheralBaseAddr  = (uint32_t)&(USART1->DR);
	DMA1_Init.DMA_DIR                 = DMA_DIR_PeripheralDST;
	DMA1_Init.DMA_PeripheralInc       = DMA_PeripheralInc_Disable;
	DMA1_Init.DMA_MemoryInc           = DMA_MemoryInc_Enable;
	DMA1_Init.DMA_PeripheralDataSize  = DMA_PeripheralDataSize_Byte;
	DMA1_Init.DMA_MemoryDataSize      = DMA_MemoryDataSize_Byte;
	DMA1_Init.DMA_Mode                = DMA_Mode_Normal;
	DMA1_Init.DMA_Priority            = DMA_Priority_Medium;
	DMA1_Init.DMA_M2M                 = DMA_M2M_Disable;
	// Сбрасываем все настройки DMA Channel4
	DMA_DeInit(DMA1_Channel4);
	// Инициализация DMA Channel4
	DMA_Init(DMA1_Channel4, &DMA1_Init);
    // Включаем прерывание по окончании 
    DMA_ITConfig(DMA1_Channel4, DMA_IT_TC, ENABLE);
}

  /* TIMER configuration *****************************************************/
void TIM_Configuration(void)
{   // частота 8 МГц

	// Структуры
	TIM_TimeBaseInitTypeDef  TIM_TimeBaseStructure;
	TIM_OCInitTypeDef        TIM_OCInitStructure;

	// Разрешить тактирование TIM2
	RCC_APB1PeriphClockCmd(RCC_APB1Periph_TIM2, ENABLE);

	// ================== Таймер TIM1 =========================================
	// Заполняем структуру
	TIM_TimeBaseStructure.TIM_Period             = 8000;
	TIM_TimeBaseStructure.TIM_Prescaler          = 2000;
	TIM_TimeBaseStructure.TIM_ClockDivision      = TIM_CKD_DIV1;
	TIM_TimeBaseStructure.TIM_CounterMode        = TIM_CounterMode_Up;
	TIM_TimeBaseStructure.TIM_RepetitionCounter  = 0;
	// Делаем базовую настройку таймерам
	TIM_TimeBaseInit(TIM2, &TIM_TimeBaseStructure);

	// Заполняем конфигурацию каналов таймера
	TIM_OCInitStructure.TIM_OCMode         = TIM_OCMode_PWM1;
	TIM_OCInitStructure.TIM_OutputState    = TIM_OutputState_Enable;
	TIM_OCInitStructure.TIM_Pulse          = 7000;
	TIM_OCInitStructure.TIM_OCPolarity     = TIM_OCPolarity_Low;
	TIM_OC3Init(TIM2, &TIM_OCInitStructure);
	TIM_OC3PreloadConfig(TIM2, TIM_OCPreload_Enable);

	TIM_OC3PreloadConfig(TIM2, TIM_OCPreload_Enable);
	TIM_ARRPreloadConfig(TIM2, ENABLE);

	// Запуск таймеров
	TIM_Cmd(TIM2, ENABLE);
}

/*-------------------------------------------------------------------------- */
void Init_STM32100(void)
{
	static uint32_t *Unique_ID = (uint32_t *)0x1FFFF7E8;
	// Начальные значения переменных

	data.ID[0] = *(Unique_ID + 0);        // ID
	data.ID[1] = *(Unique_ID + 1);        // ID
	data.ID[2] = *(Unique_ID + 2);        // ID

	data.i2c1_speed = 3;                  // I2C_SPEED_NORMAL = 200 000
	data.i2c2_speed = 3;                  // I2C_SPEED_NORMAL = 200 000

	data.sensor.sensor_error = 0x0000;    // Сброс маски ошибок чтения датчиков
	data.flags = 0x00;                    // Сброс флагов работы устройства
    data.ignore_sensor = 0x0000;          // Сброс маски ингорируемых датчиков 


	// процедуры инициализации

	RCC_Configuration();                          // Настройка тактирования ядра
	GPIO_Configuration();                         // Настройка портов ввода/вывода
	DMA_Configuration();                          // Настройка DMA
	I2C_Configuration(I2C1, data.i2c1_speed);     // Настройка модуля I2C1
	I2C_Configuration(I2C2, data.i2c2_speed);     // Настройка модуля I2C2
	USART_Configuration();                        // Настройка модуля UART
	NVIC_Configuration();                         // Настройка модуля прерываний
	TIM_Configuration();                          // Настройка таймера для индикации работы устройства

}
