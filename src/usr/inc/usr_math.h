/***********************************************************************************************/
/*         Библиотека с различными математическими функциями                                   */
/***********************************************************************************************/
#ifndef USR_MATH_H
#define USR_MATH_H

//----- Инклуды --------------------------------------------------------------------------------
#include <stdint.h>
//----- Макросы --------------------------------------------------------------------------------


//----- Прототипы функций ----------------------------------------------------------------------

uint32_t usr_sqrt(uint32_t value);                   /**< Квадратный корень числа */
uint32_t usr_abs(int32_t value);                     /**< Модуль числа */

//----- КОНЕЦ ФАЙЛА ----------------------------------------------------------------------------
#endif
