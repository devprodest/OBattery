﻿namespace GetTemperature
{
    partial class frmMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series2 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series3 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series4 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series5 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series6 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series7 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series8 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series9 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series10 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series11 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series12 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series13 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series14 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series15 = new System.Windows.Forms.DataVisualization.Charting.Series();
            System.Windows.Forms.DataVisualization.Charting.Series series16 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.tmrGetData = new System.Windows.Forms.Timer(this.components);
            this.comPort = new System.IO.Ports.SerialPort(this.components);
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.checkBox15 = new System.Windows.Forms.CheckBox();
            this.checkBox14 = new System.Windows.Forms.CheckBox();
            this.checkBox13 = new System.Windows.Forms.CheckBox();
            this.checkBox12 = new System.Windows.Forms.CheckBox();
            this.checkBox11 = new System.Windows.Forms.CheckBox();
            this.checkBox10 = new System.Windows.Forms.CheckBox();
            this.checkBox9 = new System.Windows.Forms.CheckBox();
            this.checkBox8 = new System.Windows.Forms.CheckBox();
            this.checkBox7 = new System.Windows.Forms.CheckBox();
            this.checkBox6 = new System.Windows.Forms.CheckBox();
            this.checkBox5 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.chkTestSensorNumber1 = new System.Windows.Forms.CheckBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripComboBox1 = new System.Windows.Forms.ToolStripComboBox();
            this.toolStripSeparator7 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel1 = new System.Windows.Forms.ToolStripLabel();
            this.lblID = new System.Windows.Forms.ToolStripLabel();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.btnStart = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel2 = new System.Windows.Forms.ToolStripLabel();
            this.toolStripButton1 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton2 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripLabel3 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator4 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton4 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator8 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton5 = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator5 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripButton6 = new System.Windows.Forms.ToolStripButton();
            this.toolStripButton7 = new System.Windows.Forms.ToolStripButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.nmbGeterInterval = new System.Windows.Forms.NumericUpDown();
            this.nmbMaxY = new System.Windows.Forms.NumericUpDown();
            this.label3 = new System.Windows.Forms.Label();
            this.button1 = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.nmbMinY = new System.Windows.Forms.NumericUpDown();
            this.toolStripComboBox2 = new System.Windows.Forms.ComboBox();
            this.numericUpDown1 = new System.Windows.Forms.NumericUpDown();
            this.checkBox16 = new System.Windows.Forms.CheckBox();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.chrDataGraf = new System.Windows.Forms.DataVisualization.Charting.Chart();
            this.richTextBox1 = new System.Windows.Forms.RichTextBox();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            this.toolStrip1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmbGeterInterval)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmbMaxY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmbMinY)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.chrDataGraf)).BeginInit();
            this.SuspendLayout();
            // 
            // tmrGetData
            // 
            this.tmrGetData.Tick += new System.EventHandler(this.tmrGetData_Tick);
            // 
            // comPort
            // 
            this.comPort.BaudRate = 57600;
            this.comPort.PortName = "COM12";
            this.comPort.ReadTimeout = 600;
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Location = new System.Drawing.Point(866, 28);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(97, 395);
            this.groupBox1.TabIndex = 7;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Датчики";
            this.groupBox1.Visible = false;
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.checkBox15);
            this.panel1.Controls.Add(this.checkBox14);
            this.panel1.Controls.Add(this.checkBox13);
            this.panel1.Controls.Add(this.checkBox12);
            this.panel1.Controls.Add(this.checkBox11);
            this.panel1.Controls.Add(this.checkBox10);
            this.panel1.Controls.Add(this.checkBox9);
            this.panel1.Controls.Add(this.checkBox8);
            this.panel1.Controls.Add(this.checkBox7);
            this.panel1.Controls.Add(this.checkBox6);
            this.panel1.Controls.Add(this.checkBox5);
            this.panel1.Controls.Add(this.checkBox4);
            this.panel1.Controls.Add(this.checkBox3);
            this.panel1.Controls.Add(this.checkBox2);
            this.panel1.Controls.Add(this.checkBox1);
            this.panel1.Controls.Add(this.chkTestSensorNumber1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 16);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(91, 376);
            this.panel1.TabIndex = 10;
            // 
            // checkBox15
            // 
            this.checkBox15.AutoSize = true;
            this.checkBox15.Checked = true;
            this.checkBox15.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox15.Dock = System.Windows.Forms.DockStyle.Top;
            this.checkBox15.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.checkBox15.Location = new System.Drawing.Point(0, 345);
            this.checkBox15.Name = "checkBox15";
            this.checkBox15.Padding = new System.Windows.Forms.Padding(7, 3, 3, 3);
            this.checkBox15.Size = new System.Drawing.Size(91, 23);
            this.checkBox15.TabIndex = 15;
            this.checkBox15.Tag = "16";
            this.checkBox15.Text = "ДТ16";
            this.checkBox15.UseVisualStyleBackColor = true;
            this.checkBox15.CheckedChanged += new System.EventHandler(this.CheckedChanged);
            this.checkBox15.MouseDown += new System.Windows.Forms.MouseEventHandler(this.checkBox15_MouseDown);
            this.checkBox15.MouseLeave += new System.EventHandler(this.checkBox_MouseLeave);
            this.checkBox15.MouseHover += new System.EventHandler(this.checkBox_MouseHover);
            // 
            // checkBox14
            // 
            this.checkBox14.AutoSize = true;
            this.checkBox14.Checked = true;
            this.checkBox14.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox14.Dock = System.Windows.Forms.DockStyle.Top;
            this.checkBox14.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.checkBox14.Location = new System.Drawing.Point(0, 322);
            this.checkBox14.Name = "checkBox14";
            this.checkBox14.Padding = new System.Windows.Forms.Padding(7, 3, 3, 3);
            this.checkBox14.Size = new System.Drawing.Size(91, 23);
            this.checkBox14.TabIndex = 14;
            this.checkBox14.Tag = "15";
            this.checkBox14.Text = "ДТ15";
            this.checkBox14.UseVisualStyleBackColor = true;
            this.checkBox14.CheckedChanged += new System.EventHandler(this.CheckedChanged);
            this.checkBox14.MouseDown += new System.Windows.Forms.MouseEventHandler(this.checkBox15_MouseDown);
            this.checkBox14.MouseLeave += new System.EventHandler(this.checkBox_MouseLeave);
            this.checkBox14.MouseHover += new System.EventHandler(this.checkBox_MouseHover);
            // 
            // checkBox13
            // 
            this.checkBox13.AutoSize = true;
            this.checkBox13.Checked = true;
            this.checkBox13.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox13.Dock = System.Windows.Forms.DockStyle.Top;
            this.checkBox13.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.checkBox13.Location = new System.Drawing.Point(0, 299);
            this.checkBox13.Name = "checkBox13";
            this.checkBox13.Padding = new System.Windows.Forms.Padding(7, 3, 3, 3);
            this.checkBox13.Size = new System.Drawing.Size(91, 23);
            this.checkBox13.TabIndex = 13;
            this.checkBox13.Tag = "14";
            this.checkBox13.Text = "ДТ14";
            this.checkBox13.UseVisualStyleBackColor = true;
            this.checkBox13.CheckedChanged += new System.EventHandler(this.CheckedChanged);
            this.checkBox13.MouseDown += new System.Windows.Forms.MouseEventHandler(this.checkBox15_MouseDown);
            this.checkBox13.MouseLeave += new System.EventHandler(this.checkBox_MouseLeave);
            this.checkBox13.MouseHover += new System.EventHandler(this.checkBox_MouseHover);
            // 
            // checkBox12
            // 
            this.checkBox12.AutoSize = true;
            this.checkBox12.Checked = true;
            this.checkBox12.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox12.Dock = System.Windows.Forms.DockStyle.Top;
            this.checkBox12.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.checkBox12.Location = new System.Drawing.Point(0, 276);
            this.checkBox12.Name = "checkBox12";
            this.checkBox12.Padding = new System.Windows.Forms.Padding(7, 3, 3, 3);
            this.checkBox12.Size = new System.Drawing.Size(91, 23);
            this.checkBox12.TabIndex = 12;
            this.checkBox12.Tag = "13";
            this.checkBox12.Text = "ДТ13";
            this.checkBox12.UseVisualStyleBackColor = true;
            this.checkBox12.CheckedChanged += new System.EventHandler(this.CheckedChanged);
            this.checkBox12.MouseDown += new System.Windows.Forms.MouseEventHandler(this.checkBox15_MouseDown);
            this.checkBox12.MouseLeave += new System.EventHandler(this.checkBox_MouseLeave);
            this.checkBox12.MouseHover += new System.EventHandler(this.checkBox_MouseHover);
            // 
            // checkBox11
            // 
            this.checkBox11.AutoSize = true;
            this.checkBox11.Checked = true;
            this.checkBox11.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox11.Dock = System.Windows.Forms.DockStyle.Top;
            this.checkBox11.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.checkBox11.Location = new System.Drawing.Point(0, 253);
            this.checkBox11.Name = "checkBox11";
            this.checkBox11.Padding = new System.Windows.Forms.Padding(7, 3, 3, 3);
            this.checkBox11.Size = new System.Drawing.Size(91, 23);
            this.checkBox11.TabIndex = 11;
            this.checkBox11.Tag = "12";
            this.checkBox11.Text = "ДТ12";
            this.checkBox11.UseVisualStyleBackColor = true;
            this.checkBox11.CheckedChanged += new System.EventHandler(this.CheckedChanged);
            this.checkBox11.MouseDown += new System.Windows.Forms.MouseEventHandler(this.checkBox15_MouseDown);
            this.checkBox11.MouseLeave += new System.EventHandler(this.checkBox_MouseLeave);
            this.checkBox11.MouseHover += new System.EventHandler(this.checkBox_MouseHover);
            // 
            // checkBox10
            // 
            this.checkBox10.AutoSize = true;
            this.checkBox10.Checked = true;
            this.checkBox10.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox10.Dock = System.Windows.Forms.DockStyle.Top;
            this.checkBox10.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.checkBox10.Location = new System.Drawing.Point(0, 230);
            this.checkBox10.Name = "checkBox10";
            this.checkBox10.Padding = new System.Windows.Forms.Padding(7, 3, 3, 3);
            this.checkBox10.Size = new System.Drawing.Size(91, 23);
            this.checkBox10.TabIndex = 10;
            this.checkBox10.Tag = "11";
            this.checkBox10.Text = "ДТ11";
            this.checkBox10.UseVisualStyleBackColor = true;
            this.checkBox10.CheckedChanged += new System.EventHandler(this.CheckedChanged);
            this.checkBox10.MouseDown += new System.Windows.Forms.MouseEventHandler(this.checkBox15_MouseDown);
            this.checkBox10.MouseLeave += new System.EventHandler(this.checkBox_MouseLeave);
            this.checkBox10.MouseHover += new System.EventHandler(this.checkBox_MouseHover);
            // 
            // checkBox9
            // 
            this.checkBox9.AutoSize = true;
            this.checkBox9.Checked = true;
            this.checkBox9.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox9.Dock = System.Windows.Forms.DockStyle.Top;
            this.checkBox9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.checkBox9.Location = new System.Drawing.Point(0, 207);
            this.checkBox9.Name = "checkBox9";
            this.checkBox9.Padding = new System.Windows.Forms.Padding(7, 3, 3, 3);
            this.checkBox9.Size = new System.Drawing.Size(91, 23);
            this.checkBox9.TabIndex = 9;
            this.checkBox9.Tag = "10";
            this.checkBox9.Text = "ДТ10";
            this.checkBox9.UseVisualStyleBackColor = true;
            this.checkBox9.CheckedChanged += new System.EventHandler(this.CheckedChanged);
            this.checkBox9.MouseDown += new System.Windows.Forms.MouseEventHandler(this.checkBox15_MouseDown);
            this.checkBox9.MouseLeave += new System.EventHandler(this.checkBox_MouseLeave);
            this.checkBox9.MouseHover += new System.EventHandler(this.checkBox_MouseHover);
            // 
            // checkBox8
            // 
            this.checkBox8.AutoSize = true;
            this.checkBox8.Checked = true;
            this.checkBox8.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox8.Dock = System.Windows.Forms.DockStyle.Top;
            this.checkBox8.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.checkBox8.Location = new System.Drawing.Point(0, 184);
            this.checkBox8.Name = "checkBox8";
            this.checkBox8.Padding = new System.Windows.Forms.Padding(7, 3, 3, 3);
            this.checkBox8.Size = new System.Drawing.Size(91, 23);
            this.checkBox8.TabIndex = 8;
            this.checkBox8.Tag = "9";
            this.checkBox8.Text = "ДТ9";
            this.checkBox8.UseVisualStyleBackColor = true;
            this.checkBox8.CheckedChanged += new System.EventHandler(this.CheckedChanged);
            this.checkBox8.MouseDown += new System.Windows.Forms.MouseEventHandler(this.checkBox15_MouseDown);
            this.checkBox8.MouseLeave += new System.EventHandler(this.checkBox_MouseLeave);
            this.checkBox8.MouseHover += new System.EventHandler(this.checkBox_MouseHover);
            // 
            // checkBox7
            // 
            this.checkBox7.AutoSize = true;
            this.checkBox7.Checked = true;
            this.checkBox7.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox7.Dock = System.Windows.Forms.DockStyle.Top;
            this.checkBox7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.checkBox7.Location = new System.Drawing.Point(0, 161);
            this.checkBox7.Name = "checkBox7";
            this.checkBox7.Padding = new System.Windows.Forms.Padding(7, 3, 3, 3);
            this.checkBox7.Size = new System.Drawing.Size(91, 23);
            this.checkBox7.TabIndex = 7;
            this.checkBox7.Tag = "8";
            this.checkBox7.Text = "ДТ8";
            this.checkBox7.UseVisualStyleBackColor = true;
            this.checkBox7.CheckedChanged += new System.EventHandler(this.CheckedChanged);
            this.checkBox7.MouseDown += new System.Windows.Forms.MouseEventHandler(this.checkBox15_MouseDown);
            this.checkBox7.MouseLeave += new System.EventHandler(this.checkBox_MouseLeave);
            this.checkBox7.MouseHover += new System.EventHandler(this.checkBox_MouseHover);
            // 
            // checkBox6
            // 
            this.checkBox6.AutoSize = true;
            this.checkBox6.Checked = true;
            this.checkBox6.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox6.Dock = System.Windows.Forms.DockStyle.Top;
            this.checkBox6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.checkBox6.Location = new System.Drawing.Point(0, 138);
            this.checkBox6.Name = "checkBox6";
            this.checkBox6.Padding = new System.Windows.Forms.Padding(7, 3, 3, 3);
            this.checkBox6.Size = new System.Drawing.Size(91, 23);
            this.checkBox6.TabIndex = 6;
            this.checkBox6.Tag = "7";
            this.checkBox6.Text = "ДТ7";
            this.checkBox6.UseVisualStyleBackColor = true;
            this.checkBox6.CheckedChanged += new System.EventHandler(this.CheckedChanged);
            this.checkBox6.MouseDown += new System.Windows.Forms.MouseEventHandler(this.checkBox15_MouseDown);
            this.checkBox6.MouseLeave += new System.EventHandler(this.checkBox_MouseLeave);
            this.checkBox6.MouseHover += new System.EventHandler(this.checkBox_MouseHover);
            // 
            // checkBox5
            // 
            this.checkBox5.AutoSize = true;
            this.checkBox5.Checked = true;
            this.checkBox5.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox5.Dock = System.Windows.Forms.DockStyle.Top;
            this.checkBox5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.checkBox5.Location = new System.Drawing.Point(0, 115);
            this.checkBox5.Name = "checkBox5";
            this.checkBox5.Padding = new System.Windows.Forms.Padding(7, 3, 3, 3);
            this.checkBox5.Size = new System.Drawing.Size(91, 23);
            this.checkBox5.TabIndex = 5;
            this.checkBox5.Tag = "6";
            this.checkBox5.Text = "ДТ6";
            this.checkBox5.UseVisualStyleBackColor = true;
            this.checkBox5.CheckedChanged += new System.EventHandler(this.CheckedChanged);
            this.checkBox5.MouseDown += new System.Windows.Forms.MouseEventHandler(this.checkBox15_MouseDown);
            this.checkBox5.MouseLeave += new System.EventHandler(this.checkBox_MouseLeave);
            this.checkBox5.MouseHover += new System.EventHandler(this.checkBox_MouseHover);
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Checked = true;
            this.checkBox4.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox4.Dock = System.Windows.Forms.DockStyle.Top;
            this.checkBox4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.checkBox4.Location = new System.Drawing.Point(0, 92);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Padding = new System.Windows.Forms.Padding(7, 3, 3, 3);
            this.checkBox4.Size = new System.Drawing.Size(91, 23);
            this.checkBox4.TabIndex = 4;
            this.checkBox4.Tag = "5";
            this.checkBox4.Text = "ДТ5";
            this.checkBox4.UseVisualStyleBackColor = true;
            this.checkBox4.CheckedChanged += new System.EventHandler(this.CheckedChanged);
            this.checkBox4.MouseDown += new System.Windows.Forms.MouseEventHandler(this.checkBox15_MouseDown);
            this.checkBox4.MouseLeave += new System.EventHandler(this.checkBox_MouseLeave);
            this.checkBox4.MouseHover += new System.EventHandler(this.checkBox_MouseHover);
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Checked = true;
            this.checkBox3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox3.Dock = System.Windows.Forms.DockStyle.Top;
            this.checkBox3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.checkBox3.Location = new System.Drawing.Point(0, 69);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Padding = new System.Windows.Forms.Padding(7, 3, 3, 3);
            this.checkBox3.Size = new System.Drawing.Size(91, 23);
            this.checkBox3.TabIndex = 3;
            this.checkBox3.Tag = "4";
            this.checkBox3.Text = "ДТ4";
            this.checkBox3.UseVisualStyleBackColor = true;
            this.checkBox3.CheckedChanged += new System.EventHandler(this.CheckedChanged);
            this.checkBox3.MouseDown += new System.Windows.Forms.MouseEventHandler(this.checkBox15_MouseDown);
            this.checkBox3.MouseLeave += new System.EventHandler(this.checkBox_MouseLeave);
            this.checkBox3.MouseHover += new System.EventHandler(this.checkBox_MouseHover);
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Checked = true;
            this.checkBox2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox2.Dock = System.Windows.Forms.DockStyle.Top;
            this.checkBox2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.checkBox2.Location = new System.Drawing.Point(0, 46);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Padding = new System.Windows.Forms.Padding(7, 3, 3, 3);
            this.checkBox2.Size = new System.Drawing.Size(91, 23);
            this.checkBox2.TabIndex = 2;
            this.checkBox2.Tag = "3";
            this.checkBox2.Text = "ДТ3";
            this.checkBox2.UseVisualStyleBackColor = true;
            this.checkBox2.CheckedChanged += new System.EventHandler(this.CheckedChanged);
            this.checkBox2.MouseDown += new System.Windows.Forms.MouseEventHandler(this.checkBox15_MouseDown);
            this.checkBox2.MouseLeave += new System.EventHandler(this.checkBox_MouseLeave);
            this.checkBox2.MouseHover += new System.EventHandler(this.checkBox_MouseHover);
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Checked = true;
            this.checkBox1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBox1.Dock = System.Windows.Forms.DockStyle.Top;
            this.checkBox1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.checkBox1.Location = new System.Drawing.Point(0, 23);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Padding = new System.Windows.Forms.Padding(7, 3, 3, 3);
            this.checkBox1.Size = new System.Drawing.Size(91, 23);
            this.checkBox1.TabIndex = 1;
            this.checkBox1.Tag = "2";
            this.checkBox1.Text = "ДТ2";
            this.checkBox1.UseVisualStyleBackColor = false;
            this.checkBox1.CheckedChanged += new System.EventHandler(this.CheckedChanged);
            this.checkBox1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.checkBox15_MouseDown);
            this.checkBox1.MouseLeave += new System.EventHandler(this.checkBox_MouseLeave);
            this.checkBox1.MouseHover += new System.EventHandler(this.checkBox_MouseHover);
            // 
            // chkTestSensorNumber1
            // 
            this.chkTestSensorNumber1.AutoSize = true;
            this.chkTestSensorNumber1.Checked = true;
            this.chkTestSensorNumber1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkTestSensorNumber1.Dock = System.Windows.Forms.DockStyle.Top;
            this.chkTestSensorNumber1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.chkTestSensorNumber1.Location = new System.Drawing.Point(0, 0);
            this.chkTestSensorNumber1.Name = "chkTestSensorNumber1";
            this.chkTestSensorNumber1.Padding = new System.Windows.Forms.Padding(7, 3, 3, 3);
            this.chkTestSensorNumber1.Size = new System.Drawing.Size(91, 23);
            this.chkTestSensorNumber1.TabIndex = 0;
            this.chkTestSensorNumber1.Tag = "1";
            this.chkTestSensorNumber1.Text = "ДТ1";
            this.chkTestSensorNumber1.UseVisualStyleBackColor = true;
            this.chkTestSensorNumber1.CheckedChanged += new System.EventHandler(this.CheckedChanged);
            this.chkTestSensorNumber1.MouseDown += new System.Windows.Forms.MouseEventHandler(this.checkBox15_MouseDown);
            this.chkTestSensorNumber1.MouseLeave += new System.EventHandler(this.checkBox_MouseLeave);
            this.chkTestSensorNumber1.MouseHover += new System.EventHandler(this.checkBox_MouseHover);
            // 
            // toolStrip1
            // 
            this.toolStrip1.GripStyle = System.Windows.Forms.ToolStripGripStyle.Hidden;
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripComboBox1,
            this.toolStripSeparator7,
            this.toolStripLabel1,
            this.lblID,
            this.toolStripSeparator1,
            this.btnStart,
            this.toolStripButton3,
            this.toolStripSeparator2,
            this.toolStripLabel2,
            this.toolStripButton1,
            this.toolStripButton2,
            this.toolStripSeparator3,
            this.toolStripLabel3,
            this.toolStripSeparator4,
            this.toolStripButton4,
            this.toolStripSeparator8,
            this.toolStripButton5,
            this.toolStripSeparator5,
            this.toolStripButton6,
            this.toolStripButton7});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.RenderMode = System.Windows.Forms.ToolStripRenderMode.Professional;
            this.toolStrip1.Size = new System.Drawing.Size(963, 25);
            this.toolStrip1.TabIndex = 13;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripComboBox1
            // 
            this.toolStripComboBox1.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toolStripComboBox1.Name = "toolStripComboBox1";
            this.toolStripComboBox1.Size = new System.Drawing.Size(80, 25);
            this.toolStripComboBox1.DropDown += new System.EventHandler(this.toolStripComboBox1_DropDown);
            this.toolStripComboBox1.SelectedIndexChanged += new System.EventHandler(this.toolStripComboBox1_SelectedIndexChanged);
            // 
            // toolStripSeparator7
            // 
            this.toolStripSeparator7.Name = "toolStripSeparator7";
            this.toolStripSeparator7.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel1
            // 
            this.toolStripLabel1.Name = "toolStripLabel1";
            this.toolStripLabel1.Size = new System.Drawing.Size(21, 22);
            this.toolStripLabel1.Text = "ID:";
            // 
            // lblID
            // 
            this.lblID.AutoSize = false;
            this.lblID.Name = "lblID";
            this.lblID.Size = new System.Drawing.Size(180, 22);
            this.lblID.Text = "XXXXXXXXXXXXXX";
            this.lblID.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // btnStart
            // 
            this.btnStart.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.btnStart.Enabled = false;
            this.btnStart.Image = ((System.Drawing.Image)(resources.GetObject("btnStart.Image")));
            this.btnStart.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.btnStart.Name = "btnStart";
            this.btnStart.Size = new System.Drawing.Size(41, 22);
            this.btnStart.Text = "СТАРТ";
            this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
            // 
            // toolStripButton3
            // 
            this.toolStripButton3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton3.Image")));
            this.toolStripButton3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton3.Name = "toolStripButton3";
            this.toolStripButton3.Size = new System.Drawing.Size(46, 22);
            this.toolStripButton3.Text = "СБРОС";
            this.toolStripButton3.Click += new System.EventHandler(this.toolStripButton3_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel2
            // 
            this.toolStripLabel2.Name = "toolStripLabel2";
            this.toolStripLabel2.Size = new System.Drawing.Size(75, 22);
            this.toolStripLabel2.Text = "Сохранение:";
            // 
            // toolStripButton1
            // 
            this.toolStripButton1.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton1.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton1.Image")));
            this.toolStripButton1.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton1.Name = "toolStripButton1";
            this.toolStripButton1.Size = new System.Drawing.Size(92, 22);
            this.toolStripButton1.Text = "ИЗОБРАЖЕНИЕ";
            this.toolStripButton1.Click += new System.EventHandler(this.toolStripButton1_Click);
            // 
            // toolStripButton2
            // 
            this.toolStripButton2.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton2.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton2.Image")));
            this.toolStripButton2.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton2.Name = "toolStripButton2";
            this.toolStripButton2.Size = new System.Drawing.Size(47, 22);
            this.toolStripButton2.Text = "ЧИСЛА";
            this.toolStripButton2.Click += new System.EventHandler(this.toolStripButton2_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripLabel3
            // 
            this.toolStripLabel3.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripLabel3.Image = ((System.Drawing.Image)(resources.GetObject("toolStripLabel3.Image")));
            this.toolStripLabel3.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripLabel3.Name = "toolStripLabel3";
            this.toolStripLabel3.Size = new System.Drawing.Size(66, 22);
            this.toolStripLabel3.Text = "ЗАГРУЗИТЬ";
            this.toolStripLabel3.Click += new System.EventHandler(this.toolStripLabel3_Click);
            // 
            // toolStripSeparator4
            // 
            this.toolStripSeparator4.Name = "toolStripSeparator4";
            this.toolStripSeparator4.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton4
            // 
            this.toolStripButton4.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton4.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton4.Image")));
            this.toolStripButton4.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton4.Name = "toolStripButton4";
            this.toolStripButton4.Size = new System.Drawing.Size(60, 22);
            this.toolStripButton4.Text = "ДАТЧИКИ";
            this.toolStripButton4.Click += new System.EventHandler(this.toolStripButton4_Click);
            // 
            // toolStripSeparator8
            // 
            this.toolStripSeparator8.Name = "toolStripSeparator8";
            this.toolStripSeparator8.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton5
            // 
            this.toolStripButton5.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton5.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton5.Image")));
            this.toolStripButton5.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton5.Name = "toolStripButton5";
            this.toolStripButton5.Size = new System.Drawing.Size(75, 22);
            this.toolStripButton5.Text = "НАСТРОЙКИ";
            this.toolStripButton5.Click += new System.EventHandler(this.toolStripButton5_Click);
            // 
            // toolStripSeparator5
            // 
            this.toolStripSeparator5.Name = "toolStripSeparator5";
            this.toolStripSeparator5.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButton6
            // 
            this.toolStripButton6.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton6.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton6.Image")));
            this.toolStripButton6.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton6.Name = "toolStripButton6";
            this.toolStripButton6.Size = new System.Drawing.Size(32, 22);
            this.toolStripButton6.Text = "ЛОГ";
            this.toolStripButton6.Click += new System.EventHandler(this.toolStripButton6_Click);
            // 
            // toolStripButton7
            // 
            this.toolStripButton7.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButton7.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButton7.Image")));
            this.toolStripButton7.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButton7.Name = "toolStripButton7";
            this.toolStripButton7.Size = new System.Drawing.Size(97, 22);
            this.toolStripButton7.Text = "СОХРАНИТЬ ЛОГ";
            this.toolStripButton7.Click += new System.EventHandler(this.toolStripButton7_Click);
            // 
            // panel2
            // 
            this.panel2.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.panel2.Controls.Add(this.tableLayoutPanel1);
            this.panel2.Location = new System.Drawing.Point(597, 28);
            this.panel2.Name = "panel2";
            this.panel2.Padding = new System.Windows.Forms.Padding(3);
            this.panel2.Size = new System.Drawing.Size(266, 137);
            this.panel2.TabIndex = 14;
            this.panel2.Visible = false;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 63.75F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 36.25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 71F));
            this.tableLayoutPanel1.Controls.Add(this.label5, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.label4, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.nmbGeterInterval, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.nmbMaxY, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.label3, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.button1, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.nmbMinY, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.toolStripComboBox2, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.numericUpDown1, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.checkBox16, 2, 4);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle());
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(260, 131);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Location = new System.Drawing.Point(3, 105);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(114, 26);
            this.label5.TabIndex = 13;
            this.label5.Text = "Ограничить запись:";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Location = new System.Drawing.Point(3, 78);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(114, 27);
            this.label4.TabIndex = 11;
            this.label4.Text = "Скорость I2C:";
            this.label4.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(114, 26);
            this.label1.TabIndex = 0;
            this.label1.Text = "Частота опроса:";
            this.label1.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // nmbGeterInterval
            // 
            this.nmbGeterInterval.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel1.SetColumnSpan(this.nmbGeterInterval, 2);
            this.nmbGeterInterval.DataBindings.Add(new System.Windows.Forms.Binding("Value", global::GetTemperature.Properties.Settings.Default, "GetTempInterval", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.nmbGeterInterval.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nmbGeterInterval.Increment = new decimal(new int[] {
            10,
            0,
            0,
            0});
            this.nmbGeterInterval.Location = new System.Drawing.Point(123, 3);
            this.nmbGeterInterval.Maximum = new decimal(new int[] {
            3600000,
            0,
            0,
            0});
            this.nmbGeterInterval.Minimum = new decimal(new int[] {
            1500,
            0,
            0,
            0});
            this.nmbGeterInterval.Name = "nmbGeterInterval";
            this.nmbGeterInterval.Size = new System.Drawing.Size(134, 20);
            this.nmbGeterInterval.TabIndex = 3;
            this.nmbGeterInterval.Value = global::GetTemperature.Properties.Settings.Default.GetTempInterval;
            this.nmbGeterInterval.ValueChanged += new System.EventHandler(this.nmbGeterInterval_ValueChanged);
            // 
            // nmbMaxY
            // 
            this.nmbMaxY.DataBindings.Add(new System.Windows.Forms.Binding("Value", global::GetTemperature.Properties.Settings.Default, "MAXY", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.nmbMaxY.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nmbMaxY.Location = new System.Drawing.Point(123, 29);
            this.nmbMaxY.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.nmbMaxY.Name = "nmbMaxY";
            this.nmbMaxY.Size = new System.Drawing.Size(62, 20);
            this.nmbMaxY.TabIndex = 9;
            this.nmbMaxY.Value = global::GetTemperature.Properties.Settings.Default.MAXY;
            this.nmbMaxY.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Location = new System.Drawing.Point(3, 26);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(114, 26);
            this.label3.TabIndex = 4;
            this.label3.Text = "Ось Y MAX:";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // button1
            // 
            this.button1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button1.Location = new System.Drawing.Point(191, 29);
            this.button1.Name = "button1";
            this.tableLayoutPanel1.SetRowSpan(this.button1, 2);
            this.button1.Size = new System.Drawing.Size(66, 46);
            this.button1.TabIndex = 10;
            this.button1.Text = "СБРОС";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button2_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Location = new System.Drawing.Point(3, 52);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(114, 26);
            this.label2.TabIndex = 4;
            this.label2.Text = "Ось Y MIN:";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // nmbMinY
            // 
            this.nmbMinY.DataBindings.Add(new System.Windows.Forms.Binding("Value", global::GetTemperature.Properties.Settings.Default, "MINI", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.nmbMinY.Dock = System.Windows.Forms.DockStyle.Fill;
            this.nmbMinY.Location = new System.Drawing.Point(123, 55);
            this.nmbMinY.Minimum = new decimal(new int[] {
            100,
            0,
            0,
            -2147483648});
            this.nmbMinY.Name = "nmbMinY";
            this.nmbMinY.Size = new System.Drawing.Size(62, 20);
            this.nmbMinY.TabIndex = 9;
            this.nmbMinY.Value = global::GetTemperature.Properties.Settings.Default.MINI;
            this.nmbMinY.ValueChanged += new System.EventHandler(this.numericUpDown1_ValueChanged);
            // 
            // toolStripComboBox2
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.toolStripComboBox2, 2);
            this.toolStripComboBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.toolStripComboBox2.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
            this.toolStripComboBox2.FormattingEnabled = true;
            this.toolStripComboBox2.Items.AddRange(new object[] {
            "VERYLOW",
            "LOW",
            "MIDDLE",
            "NORMAL",
            "HIGH"});
            this.toolStripComboBox2.Location = new System.Drawing.Point(123, 81);
            this.toolStripComboBox2.Name = "toolStripComboBox2";
            this.toolStripComboBox2.Size = new System.Drawing.Size(134, 21);
            this.toolStripComboBox2.TabIndex = 12;
            this.toolStripComboBox2.SelectedIndexChanged += new System.EventHandler(this.toolStripComboBox2_SelectedIndexChanged);
            // 
            // numericUpDown1
            // 
            this.numericUpDown1.DataBindings.Add(new System.Windows.Forms.Binding("Value", global::GetTemperature.Properties.Settings.Default, "MAXOldData", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.numericUpDown1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.numericUpDown1.Location = new System.Drawing.Point(123, 108);
            this.numericUpDown1.Maximum = new decimal(new int[] {
            100000000,
            0,
            0,
            0});
            this.numericUpDown1.Name = "numericUpDown1";
            this.numericUpDown1.Size = new System.Drawing.Size(62, 20);
            this.numericUpDown1.TabIndex = 14;
            this.numericUpDown1.Value = global::GetTemperature.Properties.Settings.Default.MAXOldData;
            // 
            // checkBox16
            // 
            this.checkBox16.AutoSize = true;
            this.checkBox16.Checked = global::GetTemperature.Properties.Settings.Default.DelOldData;
            this.checkBox16.DataBindings.Add(new System.Windows.Forms.Binding("Checked", global::GetTemperature.Properties.Settings.Default, "DelOldData", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.checkBox16.Location = new System.Drawing.Point(191, 108);
            this.checkBox16.Name = "checkBox16";
            this.checkBox16.Size = new System.Drawing.Size(48, 17);
            this.checkBox16.TabIndex = 15;
            this.checkBox16.Text = "ВКЛ";
            this.checkBox16.UseVisualStyleBackColor = true;
            // 
            // splitContainer1
            // 
            this.splitContainer1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.splitContainer1.DataBindings.Add(new System.Windows.Forms.Binding("Panel2Collapsed", global::GetTemperature.Properties.Settings.Default, "ShowLogWindow", true, System.Windows.Forms.DataSourceUpdateMode.OnPropertyChanged));
            this.splitContainer1.Location = new System.Drawing.Point(0, 28);
            this.splitContainer1.Name = "splitContainer1";
            this.splitContainer1.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.Controls.Add(this.chrDataGraf);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.richTextBox1);
            this.splitContainer1.Panel2Collapsed = global::GetTemperature.Properties.Settings.Default.ShowLogWindow;
            this.splitContainer1.Size = new System.Drawing.Size(963, 469);
            this.splitContainer1.SplitterDistance = 313;
            this.splitContainer1.TabIndex = 15;
            // 
            // chrDataGraf
            // 
            this.chrDataGraf.BackColor = System.Drawing.Color.Empty;
            chartArea1.AxisX.LogarithmBase = 2D;
            chartArea1.AxisX.MajorGrid.LineColor = System.Drawing.Color.LightGray;
            chartArea1.AxisY.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.True;
            chartArea1.AxisY.Interval = 10D;
            chartArea1.AxisY.IsLabelAutoFit = false;
            chartArea1.AxisY.LabelStyle.Interval = 10D;
            chartArea1.AxisY.LabelStyle.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea1.AxisY.MajorGrid.Interval = 10D;
            chartArea1.AxisY.MajorGrid.LineColor = System.Drawing.Color.LightGray;
            chartArea1.AxisY.Maximum = 60D;
            chartArea1.AxisY.MaximumAutoSize = 100F;
            chartArea1.AxisY.Minimum = -60D;
            chartArea1.AxisY.MinorGrid.Enabled = true;
            chartArea1.AxisY.MinorGrid.Interval = 1D;
            chartArea1.AxisY.MinorGrid.LineColor = System.Drawing.SystemColors.ButtonFace;
            chartArea1.AxisY.ScaleBreakStyle.Enabled = true;
            chartArea1.AxisY.ScaleBreakStyle.MaxNumberOfBreaks = 1;
            chartArea1.AxisY2.Enabled = System.Windows.Forms.DataVisualization.Charting.AxisEnabled.False;
            chartArea1.AxisY2.Interval = 5D;
            chartArea1.AxisY2.MajorGrid.Interval = 10D;
            chartArea1.AxisY2.MajorGrid.IntervalType = System.Windows.Forms.DataVisualization.Charting.DateTimeIntervalType.Number;
            chartArea1.AxisY2.Maximum = 60D;
            chartArea1.AxisY2.Minimum = -60D;
            chartArea1.CursorY.AutoScroll = false;
            chartArea1.InnerPlotPosition.Auto = false;
            chartArea1.InnerPlotPosition.Height = 96F;
            chartArea1.InnerPlotPosition.Width = 96F;
            chartArea1.InnerPlotPosition.X = 2.5F;
            chartArea1.InnerPlotPosition.Y = 1F;
            chartArea1.Name = "ChartArea1";
            chartArea1.Position.Auto = false;
            chartArea1.Position.Height = 100F;
            chartArea1.Position.Width = 100F;
            this.chrDataGraf.ChartAreas.Add(chartArea1);
            this.chrDataGraf.Dock = System.Windows.Forms.DockStyle.Fill;
            legend1.Enabled = false;
            legend1.Name = "Legend1";
            legend1.TableStyle = System.Windows.Forms.DataVisualization.Charting.LegendTableStyle.Tall;
            this.chrDataGraf.Legends.Add(legend1);
            this.chrDataGraf.Location = new System.Drawing.Point(0, 0);
            this.chrDataGraf.Margin = new System.Windows.Forms.Padding(0);
            this.chrDataGraf.Name = "chrDataGraf";
            this.chrDataGraf.Palette = System.Windows.Forms.DataVisualization.Charting.ChartColorPalette.Excel;
            series1.BorderWidth = 2;
            series1.ChartArea = "ChartArea1";
            series1.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series1.Legend = "Legend1";
            series1.Name = "ДТ1";
            series1.ToolTip = "#SERIESNAME : #VALX  - #VALY{N2}";
            series2.BorderWidth = 2;
            series2.ChartArea = "ChartArea1";
            series2.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series2.Legend = "Legend1";
            series2.Name = "ДТ2";
            series2.ToolTip = "#SERIESNAME : #VALX  - #VALY{N2}";
            series3.BorderWidth = 2;
            series3.ChartArea = "ChartArea1";
            series3.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series3.Legend = "Legend1";
            series3.Name = "ДТ3";
            series3.ToolTip = "#SERIESNAME : #VALX  - #VALY{N2}";
            series4.BorderWidth = 2;
            series4.ChartArea = "ChartArea1";
            series4.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series4.Legend = "Legend1";
            series4.Name = "ДТ4";
            series4.ToolTip = "#SERIESNAME : #VALX  - #VALY{N2}";
            series5.BorderWidth = 2;
            series5.ChartArea = "ChartArea1";
            series5.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series5.Legend = "Legend1";
            series5.Name = "ДТ5";
            series5.ToolTip = "#SERIESNAME : #VALX  - #VALY{N2}";
            series6.BorderWidth = 2;
            series6.ChartArea = "ChartArea1";
            series6.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series6.Legend = "Legend1";
            series6.Name = "ДТ6";
            series6.ToolTip = "#SERIESNAME : #VALX  - #VALY{N2}";
            series7.BorderWidth = 2;
            series7.ChartArea = "ChartArea1";
            series7.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series7.Legend = "Legend1";
            series7.Name = "ДТ7";
            series7.ToolTip = "#SERIESNAME : #VALX  - #VALY{N2}";
            series8.BorderWidth = 2;
            series8.ChartArea = "ChartArea1";
            series8.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series8.Legend = "Legend1";
            series8.Name = "ДТ8";
            series8.ToolTip = "#SERIESNAME : #VALX  - #VALY{N2}";
            series9.BorderWidth = 2;
            series9.ChartArea = "ChartArea1";
            series9.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series9.Legend = "Legend1";
            series9.Name = "ДТ9";
            series9.ToolTip = "#SERIESNAME : #VALX  - #VALY{N2}";
            series10.BorderWidth = 2;
            series10.ChartArea = "ChartArea1";
            series10.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series10.Legend = "Legend1";
            series10.Name = "ДТ10";
            series10.ToolTip = "#SERIESNAME : #VALX  - #VALY{N2}";
            series11.BorderWidth = 2;
            series11.ChartArea = "ChartArea1";
            series11.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series11.Legend = "Legend1";
            series11.Name = "ДТ11";
            series11.ToolTip = "#SERIESNAME : #VALX  - #VALY{N2}";
            series12.BorderWidth = 2;
            series12.ChartArea = "ChartArea1";
            series12.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series12.Legend = "Legend1";
            series12.Name = "ДТ12";
            series12.ToolTip = "#SERIESNAME : #VALX  - #VALY{N2}";
            series13.BorderWidth = 2;
            series13.ChartArea = "ChartArea1";
            series13.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series13.Legend = "Legend1";
            series13.Name = "ДТ13";
            series13.ToolTip = "#SERIESNAME : #VALX  - #VALY{N2}";
            series14.BorderWidth = 2;
            series14.ChartArea = "ChartArea1";
            series14.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series14.Legend = "Legend1";
            series14.Name = "ДТ14";
            series14.ToolTip = "#SERIESNAME : #VALX  - #VALY{N2}";
            series15.BorderWidth = 2;
            series15.ChartArea = "ChartArea1";
            series15.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series15.Legend = "Legend1";
            series15.Name = "ДТ15";
            series15.ToolTip = "#SERIESNAME : #VALX  - #VALY{N2}";
            series16.BorderWidth = 2;
            series16.ChartArea = "ChartArea1";
            series16.ChartType = System.Windows.Forms.DataVisualization.Charting.SeriesChartType.Line;
            series16.Legend = "Legend1";
            series16.Name = "ДТ16";
            series16.ToolTip = "#SERIESNAME : #VALX  - #VALY{N2}";
            this.chrDataGraf.Series.Add(series1);
            this.chrDataGraf.Series.Add(series2);
            this.chrDataGraf.Series.Add(series3);
            this.chrDataGraf.Series.Add(series4);
            this.chrDataGraf.Series.Add(series5);
            this.chrDataGraf.Series.Add(series6);
            this.chrDataGraf.Series.Add(series7);
            this.chrDataGraf.Series.Add(series8);
            this.chrDataGraf.Series.Add(series9);
            this.chrDataGraf.Series.Add(series10);
            this.chrDataGraf.Series.Add(series11);
            this.chrDataGraf.Series.Add(series12);
            this.chrDataGraf.Series.Add(series13);
            this.chrDataGraf.Series.Add(series14);
            this.chrDataGraf.Series.Add(series15);
            this.chrDataGraf.Series.Add(series16);
            this.chrDataGraf.Size = new System.Drawing.Size(963, 313);
            this.chrDataGraf.TabIndex = 10;
            this.chrDataGraf.GetToolTipText += new System.EventHandler<System.Windows.Forms.DataVisualization.Charting.ToolTipEventArgs>(this.chrDataGraf_GetToolTipText);
            // 
            // richTextBox1
            // 
            this.richTextBox1.BackColor = System.Drawing.SystemColors.Window;
            this.richTextBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.richTextBox1.Font = new System.Drawing.Font("Consolas", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(204)));
            this.richTextBox1.Location = new System.Drawing.Point(0, 0);
            this.richTextBox1.Name = "richTextBox1";
            this.richTextBox1.ReadOnly = true;
            this.richTextBox1.Size = new System.Drawing.Size(963, 152);
            this.richTextBox1.TabIndex = 0;
            this.richTextBox1.Text = "";
            // 
            // frmMain
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(963, 495);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.splitContainer1);
            this.DoubleBuffered = true;
            this.Name = "frmMain";
            this.Text = "GetTemperature";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            this.Load += new System.EventHandler(this.frmMain_Load);
            this.groupBox1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nmbGeterInterval)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmbMaxY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.nmbMinY)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.numericUpDown1)).EndInit();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.chrDataGraf)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.IO.Ports.SerialPort comPort;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.DataVisualization.Charting.Chart chrDataGraf;
        private System.Windows.Forms.CheckBox checkBox13;
        private System.Windows.Forms.CheckBox checkBox12;
        private System.Windows.Forms.CheckBox checkBox11;
        private System.Windows.Forms.CheckBox checkBox10;
        private System.Windows.Forms.CheckBox checkBox9;
        private System.Windows.Forms.CheckBox checkBox8;
        private System.Windows.Forms.CheckBox checkBox7;
        private System.Windows.Forms.CheckBox checkBox6;
        private System.Windows.Forms.CheckBox checkBox5;
        private System.Windows.Forms.CheckBox checkBox4;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox chkTestSensorNumber1;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripLabel toolStripLabel1;
        private System.Windows.Forms.ToolStripLabel lblID;
        private System.Windows.Forms.ToolStripButton btnStart;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripButton1;
        private System.Windows.Forms.ToolStripButton toolStripButton2;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator4;
        private System.Windows.Forms.ToolStripButton toolStripButton4;
        private System.Windows.Forms.ToolStripComboBox toolStripComboBox1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator7;
        private System.Windows.Forms.ToolStripButton toolStripLabel3;
        private System.Windows.Forms.CheckBox checkBox15;
        private System.Windows.Forms.CheckBox checkBox14;
        private System.Windows.Forms.ToolStripButton toolStripButton3;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator8;
        private System.Windows.Forms.ToolStripLabel toolStripLabel2;
        private System.Windows.Forms.NumericUpDown nmbGeterInterval;
        private System.Windows.Forms.NumericUpDown nmbMinY;
        private System.Windows.Forms.NumericUpDown nmbMaxY;
        public System.Windows.Forms.Timer tmrGetData;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox toolStripComboBox2;
        private System.Windows.Forms.ToolStripButton toolStripButton5;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.RichTextBox richTextBox1;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator5;
        private System.Windows.Forms.ToolStripButton toolStripButton6;
        private System.Windows.Forms.ToolStripButton toolStripButton7;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.NumericUpDown numericUpDown1;
        private System.Windows.Forms.CheckBox checkBox16;
    }
}

