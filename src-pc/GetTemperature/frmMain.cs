﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.IO.Ports;

namespace GetTemperature
{
    public partial class frmMain : Form
    {
        public frmMain()
        {
            InitializeComponent();
        }

        private void nmbGeterInterval_ValueChanged(object sender, EventArgs e)
        {
            tmrGetData.Interval = Decimal.ToInt32(nmbGeterInterval.Value);
        }

        private void btnStart_Click(object sender, EventArgs e)
        {
            tmrGetData.Enabled = !tmrGetData.Enabled;
            btnStart.Checked = tmrGetData.Enabled;
            tmrGetData.Interval = Decimal.ToInt32(nmbGeterInterval.Value);
            PrintLog(string.Format("{0} процесс получения данных", (btnStart.Checked) ? "ЗАПУЩЕН" : "ОСТАНОВЛЕН"));
        }

        private void tmrGetData_Tick(object sender, EventArgs e)
        {
            GetDataTemperature();
        }

        private void GetDataTemperature()
        {
            if (comPort.IsOpen)
            {
                try
                {
                    Byte[] databuff = new Byte[34];
                    comPort.Write("G");

                    comPort.Read(databuff, 0, 34);

                    for (int i = 0; i < 16; i++)
                    {
                        chrDataGraf.Series[i].Points.AddXY(DateTime.Now.ToLongTimeString(), RawToDouble(databuff[(i + 1) * 2], databuff[(i + 1) * 2 + 1]));
                        if ((checkBox16.Checked) && (chrDataGraf.Series[i].Points.Count > numericUpDown1.Value))
                        {
                            chrDataGraf.Series[i].Points.RemoveAt(0);
                        }
                    }

                    PrintLog(String.Concat(Array.ConvertAll(databuff, x => (x.ToString("X2") + " "))));
                }
                catch (Exception ex)
                {
                    PrintLog(ex.Message);
                }
            }
        }

        private void PrintLog(string str)
        {
            richTextBox1.AppendText(string.Format("{0} - {1}\n", DateTime.Now.ToLongTimeString(), str));
        }

        private Double RawToDouble(Byte b1, Byte b2)
        {
            return b1 + ((b2 == 0) ? 0 : 0.5);
        }

        private void CheckedChanged(object sender, EventArgs e)
        {
            CheckBox number = sender as CheckBox;
            chrDataGraf.Series[number.Text].Enabled = number.Checked;
        }

        private void checkBox_MouseHover(object sender, EventArgs e)
        {
            CheckBox number = sender as CheckBox;
            chrDataGraf.Series[number.Text].BorderWidth = 5;
        }

        private void checkBox_MouseLeave(object sender, EventArgs e)
        {
            CheckBox number = sender as CheckBox;
            chrDataGraf.Series[number.Text].BorderWidth = 2;

        }

        private void numericUpDown1_ValueChanged(object sender, EventArgs e)
        {

            nmbMinY.Maximum = nmbMaxY.Value - 1;
            nmbMaxY.Minimum = nmbMinY.Value + 1;

            chrDataGraf.ChartAreas[0].AxisY.Minimum = Decimal.ToDouble(nmbMinY.Value);
            chrDataGraf.ChartAreas[0].AxisY.Maximum = Decimal.ToDouble(nmbMaxY.Value);
        }

        private void button2_Click(object sender, EventArgs e)
        {
            nmbMaxY.Value = 60;
            nmbMinY.Value = -60;
        }

        private void toolStripButton1_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveImg = new SaveFileDialog();
            saveImg.Filter = @"PNG изображения|*.png";
            if (saveImg.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                chrDataGraf.SaveImage(saveImg.FileName, System.Drawing.Imaging.ImageFormat.Png);
                PrintLog(String.Format("Изображение сохранено: {0}", saveImg.FileName));
            }
        }

        private void toolStripButton4_Click(object sender, EventArgs e)
        {
            toolStripButton4.Checked = !toolStripButton4.Checked;
            groupBox1.Visible = toolStripButton4.Checked;
        }

        private void toolStripButton2_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveData = new SaveFileDialog();
            saveData.Filter = @"Данные .DATA|*.data";
            if (saveData.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(saveData.FileName, false))
                {
                    for (int i = 0; i < 16; i++)
                    {
                        file.WriteLine(String.Join("|", new List<System.Windows.Forms.DataVisualization.Charting.DataPoint>(chrDataGraf.Series[i].Points).ConvertAll(p => p.YValues[0].ToString()).ToArray()));

                    }
                }
                PrintLog(String.Format("Файл сохранен: {0}", saveData.FileName));
            }

                   
        }

        private void toolStripComboBox1_DropDown(object sender, EventArgs e)
        {
            toolStripComboBox1.Items.Clear();
            toolStripComboBox1.Items.AddRange(SerialPort.GetPortNames());
        }

        private void toolStripComboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            int[] ping = new int[4];
            int[] ID = new int[12]; 

            btnStart.Enabled = false;
            comPort.Close();
            comPort.PortName = toolStripComboBox1.SelectedItem.ToString();
            lblID.Text = "Открываем..." + comPort.PortName;
            PrintLog("Попытка открыть " + comPort.PortName);
            try
            {
                comPort.Close();
                comPort.Open();
            }
            catch (Exception ex)
            {
                comPort.Close();
                lblID.Text = "Не удалось открыть";
                PrintLog("Не удалось открыть " + comPort.PortName + " " + ex.Message);
                return;
            }
            if (comPort.IsOpen)
            {

                try
                {
                    comPort.Write("P");
                    comPort.ReadTimeout = 3000;
                    for (int i = 0; i < 4; i++)
                    {
                        ping[i] = comPort.ReadByte();
                    }
                    comPort.ReadTimeout = 800;
                }
                catch (Exception ex)
                {
                    comPort.ReadTimeout = 800;
                    comPort.Close();
                    lblID.Text = "Устройство не опознано";

                    PrintLog("Не удалось подключиться к устройству на " + comPort.PortName + " " + ex.Message);
                    return;
                }

                String str = new string(Array.ConvertAll<int, char>(ping, x => Convert.ToChar(x)));
                if (str == "pong")
                {
                    PrintLog("Подключились к устройству на " + comPort.PortName);
                    try
                    {
                        lblID.Text = "Получение ID";
                        comPort.Write("I");
                        comPort.ReadTimeout = 3000;
                        for (int i = 0; i < 12; i++)
                        {
                            ID[i] = comPort.ReadByte();
                        }
                        lblID.Text = String.Concat(Array.ConvertAll(ID, x => x.ToString("X2")));
                        btnStart.Enabled = true;
                        comPort.ReadTimeout = 800;
                        PrintLog("Получен ID устройства " + String.Concat(Array.ConvertAll(ID, x => (x.ToString("X2") + " "))));
                    }
                    catch (Exception ex)
                    {
                        comPort.ReadTimeout = 800;
                        comPort.Close();
                        lblID.Text = "Устройство не опознано";
                        PrintLog("Не удалось подключиться к устройству на " + comPort.PortName + " " + ex.Message);
                        return;
                    }
                }
                else
                {
                    comPort.Close();
                    lblID.Text = "Устройство не опознано";
                    PrintLog("Не удалось подключиться к устройству на " + comPort.PortName);
                }

            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            Properties.Settings.Default.Save();
            comPort.Close();
        }

        private void toolStripLabel3_Click(object sender, EventArgs e)
        {
            OpenFileDialog openData = new OpenFileDialog();
            openData.Filter = @"Данные .DATA|*.data";
            if (openData.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                using (System.IO.StreamReader file = new System.IO.StreamReader(openData.FileName))
                {
                    for (int i = 0; i < 16; i++)
                    {

                        Double[] Y = Array.ConvertAll(file.ReadLine().Split('|'), Double.Parse);
                        chrDataGraf.Series[i].Points.Clear();
                        for (int j = 0; j<Y.Length;j++)
                        {
                            chrDataGraf.Series[i].Points.AddY(Y[j]);
                        }
                    }
                }
                PrintLog(String.Format("Загружены данные из файла: {0}", openData.FileName));
            }
        }

        private void toolStripButton3_Click(object sender, EventArgs e)
        {
            for (int i = 0; i < 16; i++)
            {
                chrDataGraf.Series[i].Points.Clear();
            }
            PrintLog("Данные графика стёрты");
        }

        private void toolStripComboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (comPort.IsOpen)
            {
                comPort.Write(toolStripComboBox2.SelectedItem.ToString().Substring(0, 1));
                PrintLog(String.Format("Установлена скорость работы датчиков температуры: {0}", toolStripComboBox2.SelectedItem.ToString()));
            }
            else
            {
                PrintLog("Нельзя установить скорость, отсутствует подключение к устройству");
            }
        }

        private void checkBox15_MouseDown(object sender, MouseEventArgs e)
        {
            if (e.Button == System.Windows.Forms.MouseButtons.Right)
            {
                ColorDialog color = new ColorDialog();
                if (color.ShowDialog() == System.Windows.Forms.DialogResult.OK)
                {
                    chrDataGraf.Series[(sender as CheckBox).Text].Color = color.Color;
                    (sender as CheckBox).ForeColor = color.Color;
                }
            }
        }

        private void chrDataGraf_GetToolTipText(object sender, System.Windows.Forms.DataVisualization.Charting.ToolTipEventArgs e)
        {
            if ((e.Text != " ") && (e.Text != ""))
            {
                for (int i = 0; i < 16; i++)
                {
                    chrDataGraf.Series[i].BorderWidth = 2;
                }
                this.Text = e.Text.Substring(0, 4).Trim();
                chrDataGraf.Series[e.Text.Substring(0, 4).Trim()].BorderWidth = 5;

            }
            else
            {
                for (int i = 0; i < 16; i++)
                {
                    chrDataGraf.Series[i].BorderWidth = 2;
                }
            }
            
        }

        private void toolStripButton5_Click(object sender, EventArgs e)
        {
            toolStripButton5.Checked = !toolStripButton5.Checked;
            panel2.Visible = toolStripButton5.Checked;
        }

        private void toolStripButton6_Click(object sender, EventArgs e)
        {
            toolStripButton6.Checked = !toolStripButton6.Checked;
            splitContainer1.Panel2Collapsed = !toolStripButton6.Checked;
        }

        private void frmMain_Load(object sender, EventArgs e)
        {
            PrintLog("Приложение запущено");

            chrDataGraf.ChartAreas[0].AxisY.Minimum = Decimal.ToDouble(nmbMinY.Value);
            chrDataGraf.ChartAreas[0].AxisY.Maximum = Decimal.ToDouble(nmbMaxY.Value);
            toolStripButton6.Checked = !splitContainer1.Panel2Collapsed;
        }

        private void toolStripButton7_Click(object sender, EventArgs e)
        {
            SaveFileDialog saveLog = new SaveFileDialog();
            saveLog.Filter = @"LOG файл|*.log|Текстовый документ|*.txt|Все файл|*.*";
            if (saveLog.ShowDialog() == System.Windows.Forms.DialogResult.OK)
            {
                
                using (System.IO.StreamWriter file = new System.IO.StreamWriter(saveLog.FileName, false))
                {
                    file.Write(richTextBox1.Text);
                }
                PrintLog(String.Format("Лог сохранен: {0}", saveLog.FileName));
            }
        }
    }
}

                   